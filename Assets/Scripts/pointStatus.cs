﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pointStatus : MonoBehaviour
{

    [SerializeField] public bool busy;
    
    void Start()
    {
        busy = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setBussy()
    {
        busy = true;
        Debug.Log(gameObject.name + " status: " + busy);
    }

    internal void setFree()
    {
        busy = false;
        Debug.Log(gameObject.name + " status: " + busy);
    }
}
