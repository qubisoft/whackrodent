﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Monsters Wave Config")]
public class MonsterConfig : ScriptableObject
{

    [SerializeField] GameObject monsterPrefab;
    [SerializeField] GameObject StartPointsPrefab;
    [SerializeField] GameObject StayPointsPrefab;
    [SerializeField] float timeBetweenSpawns = 0.5f;
    [SerializeField] float spawnRandomFactor = 0.3f;
    [SerializeField] int numberOfMonsters = 5;
    [SerializeField] float moveSpeed = 2f;

    [SerializeField] int stepsCount = 2;


    [Header("Position Settings")]
    [SerializeField] int minStayOnPosition = 4;
    [SerializeField] int maxStayOnPosition = 10;
    [SerializeField] bool randomStayOnPos = true;

    public GameObject GetMonsterPrefab() { return monsterPrefab; }

    //public void setStayPointBussy(int index)
    //{
    //    var xxx = StayPointsPrefab.transform.GetChild(index).GetComponent<pointStatus>();
    //    xxx.setBussy();
    //}

    public List<Transform> GetWaypoints()
    {                     
        var waveWaypoints = new List<Transform>();
        try
        {

            //START

            int startPointIndex = Random.Range(0, StartPointsPrefab.transform.childCount);
            var startPoint = StartPointsPrefab.transform.GetChild(startPointIndex);

            waveWaypoints.Add(startPoint);


            //MIDDLE
            for(int i=0; i<=stepsCount-1; i++)
            {
                int stayPointIndex = Random.Range(0, StayPointsPrefab.transform.childCount);
                var stayPoint = StayPointsPrefab.transform.GetChild(stayPointIndex);
                waveWaypoints.Add(stayPoint);
            }

            //STOP

            int stopPointIndex = Random.Range(0, StartPointsPrefab.transform.childCount);
            var stopPoint = StartPointsPrefab.transform.GetChild(startPointIndex);
            waveWaypoints.Add(stopPoint);

        }
        catch (System.Exception e)
        {
            var mess = e.Message;

        }
        return waveWaypoints;

    }

    public GameObject GetStayPointsObjects() { return StayPointsPrefab; }
    public GameObject GetStartPointsObjects() { return StartPointsPrefab; }
    public float GetTimeBetweenSpawns() { return timeBetweenSpawns; }
    public float GetSpawnRandomFactor() { return spawnRandomFactor; }
    public int GetNumberOfMonsters() { return numberOfMonsters; }
    public float GetMoveSpeed() { return moveSpeed; }

    public int GetMinStayOnPosition() { return minStayOnPosition; }
    public int GetMaxStayOnPosition() { return maxStayOnPosition; }

    public bool GetRandomStayOnPosition() { return randomStayOnPos; }

}
