﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MonsterPathing : MonoBehaviour
{
    MonsterConfig monsterWaveConfig;
    List<Transform> waypoints;
    int waypointIndex = 0;
    pointStatus[] pointObjects;    
    Transform targetPoint; 
    Animator myAnimator;
    int Ready = 1;

    int minStayOnPosition = 1;
    int maxStayOnPosition = 5;
    bool timeOnPosIsRandom = true;
    [Range(1f, 5f)] [SerializeField] float timeSpendOnPosition;

    // Start is called before the first frame update
    void Start()
    {
        waypoints = monsterWaveConfig.GetWaypoints();        
        transform.position = waypoints[waypointIndex].position;        
        pointObjects = FindObjectsOfType<pointStatus>();
        myAnimator = GetComponent<Animator>();



        //time Spend on Position
        minStayOnPosition = monsterWaveConfig.GetMinStayOnPosition();
        maxStayOnPosition = monsterWaveConfig.GetMaxStayOnPosition();
        timeOnPosIsRandom = monsterWaveConfig.GetRandomStayOnPosition();
    }

   

    // Update is called once per frame
    void Update()
    {
        if (Ready == 1)
        {
            Move();
        }
    }

    public void SetWaveConfig(MonsterConfig waveConfig)
    {
        this.monsterWaveConfig = waveConfig;
    }

    public void Move()    {
        
        if (waypointIndex <= waypoints.Count - 1) // Jeśli ostatnie to kasuj
        {
            Debug.Log("current index bafore: " + waypointIndex);
            targetPoint = waypoints[waypointIndex];
            float step = monsterWaveConfig.GetMoveSpeed() * Time.deltaTime;
            //setTargetPoint_Busy(targetPoint.name, true);
            transform.position = Vector2.MoveTowards(transform.position, targetPoint.transform.position, step);



            if (transform.position == targetPoint.transform.position)
            {
                if (myAnimator) {myAnimator.SetBool("landing", true); }

                timeSpendOnPosition -= Time.deltaTime;  // T.dt is secs since last update

                if (timeSpendOnPosition <= 0)
                {

                    var next = waypointIndex + 1;
                    for (int i = waypointIndex + 1; i <= waypoints.Count - 1; i++)
                    {
                        if (targetPointIsBusy(waypoints[i]) == false) // zajeta = true
                        {
                            setTargetPoint_Busy(targetPoint.name, false);
                            if (myAnimator) { myAnimator.SetBool("landing", false); }

                            waypointIndex = i;
                            setTargetPoint_Busy(waypoints[waypointIndex].name, true);
                            timeSpendOnPosition = setTimeSpetOnPoint(minStayOnPosition, maxStayOnPosition, timeOnPosIsRandom);
                            break;
                        }
                    }
                    if (next == waypoints.Count)
                    {
                        waypointIndex++;
                    }

                }
            }

            Debug.Log("current index after: " + waypointIndex);

        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void setReady(int value)
    {
        Ready = value;
    }

    private void setNextPosition()
    {
        throw new NotImplementedException();
    }

    private bool targetPointIsBusy(Transform target)
    {
        bool status = false;

        foreach (pointStatus obj in pointObjects)
        {
            if (obj.name == target.name)
            {                
               status = obj.busy;                
            }
        }

        return status;
    }

    private void setTargetPoint_Busy(string name, bool busy)
    {
        foreach (pointStatus obj in pointObjects)
        {
            if (obj.name == name)
            {
                if (busy)
                {
                    obj.setBussy();
                }
                else
                {
                    obj.setFree();
                }                                
            }
        }
    }

    private float setTimeSpetOnPoint(int min, int max, bool isRandom)
    {
        if (isRandom)
        {
            timeSpendOnPosition = UnityEngine.Random.Range(min, max);
            return timeSpendOnPosition;
        }
        return max;
    }
}

