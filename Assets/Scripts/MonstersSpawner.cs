﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonstersSpawner : MonoBehaviour
{
    [SerializeField] List<MonsterConfig> monsterWaveConfig;
    [SerializeField] int startingWave = 0;
    [SerializeField] bool looping = false;

    IEnumerator Start()
    {

        
        

        do
        {
            yield return StartCoroutine(SpawnAllWaves(monsterWaveConfig));

        } while (looping);

        //var currentWave = waveConfigs[startingWave];
        //StartCoroutine(SpawnAllEnemiesInWave(currentWave));
    }

    private IEnumerator SpawnAllWaves(List<MonsterConfig> monsterWaveConfig)
    {
        for(int wave = startingWave; wave < monsterWaveConfig.Count; wave++)
        {
            var currentWave = monsterWaveConfig[wave];
            yield return StartCoroutine(SpawnAllMonstersInWave(currentWave));
        }
    }

    private IEnumerator SpawnAllMonstersInWave(MonsterConfig monsterConfig)
    {
        for (int monsterCount = 0; monsterCount < monsterConfig.GetNumberOfMonsters(); monsterCount++)
        {
            var monstersCount = FindObjectsOfType<MonsterPathing>().Length;
            var pointObjects = FindObjectsOfType<pointStatus>().Length;

            if (monstersCount < pointObjects)
            {
                //var newMonster = Instantiate(monsterConfig.GetMonsterPrefab(), monsterConfig.GetStartPointsObjects().transform.GetChild(0).transform.position, Quaternion.identity);
                var newMonster = Instantiate(monsterConfig.GetMonsterPrefab(), monsterConfig.GetWaypoints()[0].transform.position, Quaternion.identity);
                newMonster.GetComponent<MonsterPathing>().SetWaveConfig(monsterConfig);
            }

            yield return new WaitForSeconds(monsterConfig.GetTimeBetweenSpawns());
        }
    }
}
